module Defs where

import Util

import Control.Applicative

import Data.List
import Data.Char
import Data.Maybe (fromJust)

type Ident = String
type Error = String
type ProgCount = Int

data Reg = HReg Int | TReg Int deriving (Show, Eq, Ord)         
data Op = OpNop | OpAdd | OpSub | OpRio | OpLdi | OpLw  | OpSw
        | OpLwl | OpSwu | OpBeq | OpBne | OpBlt | OpJmp
        deriving (Show, Eq)

data ArgType = ArgRel | ArgAbs | ArgReg | ArgImm deriving Show
data OpDef = OpDef { getOp   :: Op
                   , getName :: String
                   , getArgs :: [ArgType]}
                   deriving Show

parseReg :: String -> Maybe Reg
parseReg (x:xs) = parse HReg 'H' 2 <|> parse TReg 'T' 14
    where
        parse con char lim | x == char, Just n <- readIntR 0 (lim-1) xs = Just (con n)
        parse _ _ _ = Nothing

parseOp :: String -> Maybe OpDef
parseOp s = find ((==s) . getName) opDefs

getOpDef :: Op -> OpDef
getOpDef op = fromJust . find ((==op) . getOp) $ opDefs

opDefs :: [OpDef]
opDefs = [OpDef OpNop "nop" []
         ,OpDef OpAdd "add" [r,r,r]
         ,OpDef OpSub "sub" [r,r,r]
         ,OpDef OpRio "rio" [r]
         ,OpDef OpLdi "ldi" [r,i]
         ,OpDef OpLw  "lw"  [r,r]
         ,OpDef OpSw  "sw"  [r,r]
         ,OpDef OpLwl "lwl" [r,r]
         ,OpDef OpSwu "swu" [r,r]
         ,OpDef OpBeq "beq" [r,r,jr]
         ,OpDef OpBne "bne" [r,r,jr]
         ,OpDef OpBlt "blt" [r,r,jr]
         ,OpDef OpJmp "jmp" [ja]]
            where
                r = ArgReg
                i = ArgImm
                jr = ArgRel
                ja = ArgAbs