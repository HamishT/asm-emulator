{-# LANGUAGE TupleSections, LambdaCase #-}

module Semantic (ExecInfo(..), asmSemantic) where

import Defs
import Syntax
import Environment
import Util

import Control.Monad ((>=>))

import Data.List
import Data.Maybe (maybe)
import Data.Bifunctor (bimap)

data ExecInfo = ExInf ProgCount Stmt deriving Show

asmSemantic :: [Stmt] -> Either [Error] [ExecInfo]
asmSemantic = extractVars . buildInfo >=> uncurry substituteVars
    >=> matchArgTypes >=> checkJumpsValid >=> return . filterInstructions

buildInfo :: [Stmt] -> [ExecInfo]
buildInfo = snd . mapAccumL incOnInst 0
    where
        incOnInst p s = let p' = if isInstruction s then p+1 else p
                        in (p', ExInf p s)

extractVars :: [ExecInfo] -> Either [Error] (Env, [ExecInfo])
extractVars = alignTuple . mapAccumL accumEnv emptyEnv
    where
        alignTuple (e,es) = (e,) <$> eitherPartition es
        accumEnv e inf@(ExInf p s) =
            let envOrError = either (\err -> (e, Left err)) (\env -> (env, Right inf))
            in case s of
                RegDeclStmt k v -> envOrError $ insertReg k v e 
                LabelDeclStmt k -> envOrError $ insertLabel k p e
                _               -> (e, Right inf)
        

substituteVars :: Env -> [ExecInfo] -> Either [Error] [ExecInfo]
substituteVars env = eitherPartition . fmap tryReplaceStmt
    where
        tryReplaceStmt :: ExecInfo -> Either Error ExecInfo
        tryReplaceStmt inf@(ExInf p s) = case s of
            InstStmt op args -> bimap (intercalate "; ") (ExInf p . InstStmt op) $ eitherPartition (tryReplaceArg <$> args)
            _                -> Right inf
        tryReplaceArg :: SArg -> Either Error SArg
        tryReplaceArg = \case
            SRegVar ident -> maybe (Left . rnodec $ ident) (Right . SRegLit) $ lookupReg ident env
            SLabel  ident -> maybe (Left . lnodec $ ident) (Right . SProgCount) $ lookupLabel ident env
            a             -> Right a
        rnodec, lnodec :: Ident -> Error
        rnodec ident = "Register variable '" ++ ident ++ "' not declared"
        lnodec ident = "Label '" ++ ident ++ "' not declared"

matchArgTypes :: [ExecInfo] -> Either [Error] [ExecInfo]
matchArgTypes = eitherPartition . fmap matchStmt
    where
        matchStmt :: ExecInfo -> Either Error ExecInfo
        matchStmt inf@(ExInf _ s) = case s of
            InstStmt op args -> let exps = getArgs (getOpDef op)
                                    lengthMatch = length args == length exps
                                    typeMatch   = and $ zipWith matchArg args exps
                                in if lengthMatch && typeMatch then Right inf else Left (matchErr exps)
            _ -> Right inf
        matchArg :: SArg -> ArgType -> Bool
        matchArg a b = case (a,b) of
            (SRegLit    _, ArgReg) -> True
            (SProgCount _, ArgAbs) -> True
            (SProgCount _, ArgRel) -> True
            (SImm       _, ArgImm) -> True
            _                      -> False
        matchErr :: [ArgType] -> Error
        matchErr exps = let expdef = intercalate "," $ show <$> exps
                        in "Instruction argument type error, expected: " ++ expdef

-- Validate and replace relative jumps. Return error if any out of range
checkJumpsValid :: [ExecInfo] -> Either [Error] [ExecInfo]
checkJumpsValid = eitherPartition . fmap checkStmt
    where
        checkStmt :: ExecInfo -> Either Error ExecInfo
        checkStmt inf@(ExInf p s) = case s of
            InstStmt op args -> let exps = getArgs (getOpDef op)
                                    checked = eitherPartition $ zipWith (checkArg p) exps args
                                in bimap (intercalate "; ") (ExInf p . InstStmt op) $ checked
            _                -> Right inf
        checkArg :: ProgCount -> ArgType -> SArg -> Either Error SArg
        checkArg p0 ArgRel (SProgCount p1) = let delta = p1 - p0 in if not (intRange (-8) 7 delta)
            then Left $ "Relative jump out of range (expected -8..7, got " ++ show delta ++ ")"
            else Right $ SProgDelta delta
        checkArg _ _ arg = Right arg

filterInstructions :: [ExecInfo] -> [ExecInfo]
filterInstructions = filter $ \(ExInf _ s) -> isInstruction s

isInstruction :: Stmt -> Bool
isInstruction (InstStmt _ _) = True
isInstruction _              = False