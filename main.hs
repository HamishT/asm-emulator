{-# LANGUAGE TupleSections, LambdaCase, CPP #-}

module Main (main) where

import Defs
import Lexer
import Syntax
import Semantic
import Emulator
import Util

import Control.Exception (IOException, try)
import Control.Monad ((>=>))
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Cont
import Control.Arrow ((&&&))

import Data.Either (either)
import Data.Function (fix)
import Data.Monoid ((<>))
import Data.Char (ord)
import Data.List
import Data.Maybe (maybe)

type ActionRet = (String,AppState)
type ActionCont = ContT () IO ActionRet
type ExitFunc = ActionCont
type Action = AppState -> [String] -> ActionCont

data ActDef = ActDef { getCmds::[String], getAct::Action, getHelp::String }
data AppState = AppState { getExit::ExitFunc, getEmu::EmuState, getInitEmu::EmuState, getBreaks::[Int] }

runParse :: String -> Either [Error] [ExecInfo]
runParse = asmLex >=> asmSyntax >=> asmSemantic

appEmpty :: ExitFunc -> AppState
appEmpty exit = AppState exit emptyState emptyState []

-- experimental version using continuations (via ContT transformer)
-- conclusion: lots of machinery just to supply an exit function for 'quit'
-- possibility: simplify using ExceptT instead (but still requires 'lift $' everywhere)

main :: IO ()
main = do
    putStrLn  $ "3211 ASM emulator, compiled (" ++ __DATE__ ++ "). Enter 'h' or 'help' for commands."
    let loop s0 = flip fix s0 $ \rec st -> repl st >>= rec
    evalContT $ callCC $ \exit -> loop (appEmpty $ exit ())

repl :: AppState -> ContT () IO AppState
repl st = do
    lift $ putStr "> "
    wl <- lift $ words <$> getLine
    case wl of
        []         -> lift $ return st
        (cmd:args) -> case getAct <$> findCmd cmd of
            Nothing  -> sayAndRet ("Unrecognised command '" ++ cmd ++ "'") st
            Just act -> act st args >>= uncurry sayAndRet
    where
        sayAndRet msg st' = lift $ putStrLn msg >> return st'

-- repl :: AppState -> ContT () IO AppState
-- repl st = do
--     lift $ putStr "> "
--     wl <- lift $ words <$> getLine
--     either lift id $ do
--         let attempt val err = maybe (Left $ err >> return st) Right val
--         (cmd,args) <- attempt (uncons wl) (return ())
--         act <- attempt (getAct <$> findCmd cmd) $ putStrLn ("Unrecognised command '" ++ cmd ++ "'")
--         return $ do
--             (msg,st') <- act st args
--             lift $ putStrLn msg
--             lift $ return st'

cmds :: [ActDef]
cmds = [ActDef ["h" ,"help"]     doHelp       " [cmd]: show commands, or get help for a specific command"
       ,ActDef ["q" ,"quit"]     doQuit       ": terminate the emulator"
       ,ActDef ["sh","show"]     doShow       ": show emulator state"
       ,ActDef ["re","reset"]    doReset      ": reset machine state (including program, data and IO"
       ,ActDef ["ra","restart"]  doRestart    ": restart execution of machine"
       ,ActDef ["r" ,"run"]      doRun        " [n]: run machine until a breakpoint, or at most n emulation steps"
       ,ActDef ["s" ,"step"]     doStep       ": run a single emulation step"
       ,ActDef ["lp","loadprog"] doLoadProg   " <file> [v]: parse the given file and load it into the machine; v = verbose"
       ,ActDef ["ld","loaddata"] doLoadData   " <str> [addr]: load a string into memory at this address (or 0)"
       ,ActDef ["li","loadio"]   doLoadIobuf  " <str>: load a string into the IO buffer"
       ,ActDef ["b" ,"break"]    doSetBreak   " <addr>: set a breakpoint at this address"
       ,ActDef ["ub","unbreak"]  doUnsetBreak " [n]: show breakpoints or unset one ('*' to unset all)"]

findCmd :: String -> Maybe ActDef
findCmd c = find (any (==c) . getCmds) cmds

liftRet :: ActionRet -> ActionCont
liftRet = lift . return

doHelp :: Action
doHelp st args = liftRet . (,st) $ case args of
    []    -> "Commands:\n" ++ unlines (fmap joinCmds cmds)
    (s:_) -> maybe "help: no such command" (joinCmds <> getHelp) $ findCmd s
    where
        joinCmds ad = intercalate "/" (getCmds ad)

doQuit :: Action
doQuit st _ = lift (putStrLn "Exiting") >> getExit st

doShow :: Action
doShow st _ = liftRet (prettyState $ getEmu st, st)

doReset :: Action
doReset st _ = liftRet ("Machine reset", appEmpty (getExit st))

doRestart :: Action
doRestart st _ = liftRet ("Restarting execution", st { getEmu = getInitEmu st})

doRun :: Action
doRun st args = liftRet $ case args of
    []                                       -> runAndPrint (-1)
    (ns:_) | Just n <- readInt ns, n >= 0 -> runAndPrint n
           | otherwise                       -> (argErr, st)
    where
        runAndPrint n = showStatus &&& id $ runNSteps n st
        argErr = "Invalid number of steps: must be zero-or-postive integer"


doStep :: Action
doStep st _ = liftRet $ showStatus &&& id $ (runNSteps 1 st)

doLoadProg :: Action
doLoadProg st args = case args of
    []        -> liftRet ("File path required", st)
    (f:"v":_) -> loadAndParse f True
    (f:_)     -> loadAndParse f False
    where
        loadAndParse file verbose = lift $ either handler (process verbose . runParse) <$> try (readFile file)
        handler e = ("IO exception: " ++ show (e :: IOException), st)
        process v = either
            (\errs -> ("Compilation errors:\n" ++ show errs, st))
            (\infs -> (if v then (showProg infs) else "", setEmuStates (loadProg infs) st))

doLoadData :: Action
doLoadData st args = liftRet $ case args of 
    []    -> ("Data (string) required", st)
    (d:_) -> ("", setEmuStates (loadData 0 (ord <$> d)) st)

doLoadIobuf :: Action
doLoadIobuf st args = liftRet $ case args of 
    []    -> ("Data (string) required", st)
    (d:_) -> ("", setEmuStates (loadIobuf (ord <$> d)) st)

doSetBreak :: Action
doSetBreak st args = liftRet $ case args of
    []                             -> ("Breakpoint address required", st)
    (b:_) | Just a <- readInt b -> ("Break " ++ show n ++ " set at addr " ++ b, st { getBreaks = bs++[a] })
          | otherwise              -> ("Invalid break address (must be zero-or-postive integer)", st)
    where
        bs = getBreaks st
        n = length bs

doUnsetBreak :: Action
doUnsetBreak st args = liftRet $ case args of
    (b:_) | Just n <- readInt b
          , Just v <- bs !? n -> (unsetMsg n v, unsetSt n)
          | otherwise         -> ("Invalid break index (enter 'unbreak' to list)", st)
    []                        -> ("Breakpoints: " ++ show (zip [0..] bs), st)
    where
        bs = getBreaks st
        unsetMsg n v = "Unset breakpoint " ++ show n ++ " (" ++ show v ++ ")"
        unsetSt n = st { getBreaks = listDelete n bs }

setEmuStates :: (EmuState -> EmuState) -> AppState -> AppState
setEmuStates f st = let emu = f (getEmu st) in st { getEmu = emu, getInitEmu = emu }

showStatus :: AppState -> String
showStatus st = prettyState . getEmu $ st

runNSteps :: Int -> AppState -> AppState
runNSteps m st = update $ until mustStop performStep (getEmu st, m, False)
    where
        update      (e,_,_)   = st { getEmu = e }
        mustStop    (e,n,brk) = n == 0 || not (canStep e) || (brk && elem (getProgCount e) (getBreaks st))
        performStep (e,n,_)   = (stepState e, if n<0 then n else n-1, True)

showProg :: [ExecInfo] -> String
showProg = unlines . fmap (("    "++) . show)
    