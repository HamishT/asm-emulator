module Environment (Env, emptyEnv, lookupReg, lookupLabel, insertReg, insertLabel) where

import Defs (Reg, Ident, Error, ProgCount)

import Data.Map.Strict (Map, (!?), empty, insert, notMember)

data Env = Env { envRegs :: Map Ident Reg, envLabels :: Map Ident ProgCount } deriving Show

emptyEnv :: Env
emptyEnv = Env empty empty

lookupReg :: Ident -> Env -> Maybe Reg
lookupReg n e = envRegs e !? n
lookupLabel :: Ident -> Env -> Maybe ProgCount
lookupLabel n e = envLabels e !? n

insertReg :: Ident -> Reg -> Env -> Either Error Env
insertReg k v (Env mr ml) = fmap (flip Env ml) $ checkDup k v mr
insertLabel :: Ident -> ProgCount -> Env -> Either Error Env
insertLabel k v (Env mr ml) = fmap (Env mr) $ checkDup k v ml

checkDup :: Ident -> t -> Map Ident t -> Either Error (Map Ident t)
checkDup k v m = if notMember k m
                     then Right $ insert k v m
                     else Left $ "Duplicate variable name '" ++ (show k) ++ "'."