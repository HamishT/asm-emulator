# Assembly parser and processor emulator REPL #

In a computer architecture course some time back I hacked together an assembly parser for an assignment. While learning Haskell I decided to recreate it as a 'proper' parser instead of a regex mess and this is the result.

The language itself is typical assembly for a limited instruction set and the parser adds register and label variables, as well as semantic checks for argument types and branch distances.

## Dependencies
(Sorry, no cabal setup on this one)

* containers-0.5.11.0 
* transformers-0.5.5.0 