{-# LANGUAGE LambdaCase #-}

module Syntax (asmSyntax, Stmt(..), SArg(..)) where

import Lexer
import Defs
import Util

import Data.List
import Data.Maybe
import Data.Function (fix)

data SArg = SRegVar Ident
          | SRegLit Reg
          | SLabel Ident
          | SProgCount ProgCount
          | SProgDelta Int
          | SImm Int
          deriving Show
data Stmt = RegDeclStmt Ident Reg
          | LabelDeclStmt Ident
          | InstStmt Op [SArg]
          deriving Show

asmSyntax :: [Token] -> Either [Error] [Stmt]
asmSyntax = eitherPartition . go
    where
        go = \case
            []                     -> []
            TkLineSep:ts           -> go ts
            (TkOpcode op):ts       -> let (ts1,ts2) = span isArg ts
                                      in Right (InstStmt op (mkArg <$> ts1)) : go ts2
            (TkLabel lname):ts     -> Right (LabelDeclStmt lname) : go ts
            (TkRegVar rname):t:ts  -> case t of TkRegLit rval -> Right (RegDeclStmt rname rval) : go ts
                                                _             -> [Left regvarErr]
        regvarErr = "Expected register literal in alias defintion"

mkArg :: Token -> SArg
mkArg = fromMaybe (error "fatal: syntax conversion from token to statement arg") . tryArg

isArg :: Token -> Bool
isArg = isJust . tryArg

tryArg :: Token -> Maybe SArg
tryArg = \case
    TkRegLit reg  -> Just $ SRegLit reg
    TkRegVar name -> Just $ SRegVar name
    TkLabel name  -> Just $ SLabel name
    TkNumLit num  -> Just $ SImm num
    _             -> Nothing