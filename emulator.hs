{-# LANGUAGE PatternSynonyms #-}

module Emulator (EmuState(..), RegFile, InstMem, DataMem,
    emptyState, canStep, stepState, prettyState,
    loadData, loadProg, loadIobuf) where

import Defs
import Syntax
import Semantic

import qualified Data.Map.Strict as MM
import qualified Data.IntMap.Strict as IM
import Data.List
import Data.Char (ord, chr)
import Control.Arrow ((&&&))
import Data.Maybe (isJust, fromMaybe, listToMaybe)
import Text.Printf (printf)

type RegFile = MM.Map Reg Int
type InstMem = IM.IntMap ExecInfo
type DataMem = IM.IntMap Int

data EmuState = EmuState { getProgCount :: ProgCount
                         , getRegisters :: RegFile
                         , getInstMem :: InstMem
                         , getDataMem :: DataMem
                         , getIOBuf :: [Int] }
                         deriving Show

loadProg :: [ExecInfo] -> EmuState -> EmuState
loadProg is st = st { getInstMem = IM.fromList $ zip [0..] is }

loadData :: Int -> [Int] -> EmuState -> EmuState
loadData at vs st = let replace = IM.fromList $ zip [at..] vs
                    in  st { getDataMem = IM.union replace (getDataMem st) }

loadIobuf :: [Int] -> EmuState -> EmuState
loadIobuf vs st = st { getIOBuf = vs }

emptyState :: EmuState
emptyState = EmuState 0 (mkRegs 2 14) (IM.empty) (mkData 256) []
    where
        mkData n = IM.fromList $ zip [0..(n-1)] (repeat 0)
        mkHRegs h = fmap (HReg &&& id) [0..(h-1)]
        mkTRegs t = fmap (TReg &&& const 0) [0..(t-1)]
        mkRegs h t = MM.fromList $ mkHRegs h ++ mkTRegs t

canStep :: EmuState -> Bool
canStep (EmuState pc _ im _ _) = isJust $ im IM.!? pc

stepState :: EmuState -> EmuState
stepState st@(EmuState pc rf im dm io) = EmuState pc' rf' im dm' io'
    where
        (ExInf p (InstStmt op args)) = im IM.! pc -- TODO: overflow handling
        pc' = fromMaybe (pc + 1) $ checkBranch st p op args
        rf' = checkRegisters st op args
        dm' = checkDataMem st op args
        io' = case op of { OpRio -> drop 1 io; _ -> io }

readReg :: Reg -> EmuState -> Int
readReg r st = (getRegisters st) MM.! r
writeReg :: Reg -> Int -> EmuState -> RegFile
writeReg r v st = let rf = getRegisters st  in case r of
    HReg _ -> rf -- don't overwrite hard registers
    TReg _ -> MM.insert r (v `mod` 256) rf

readMem :: Int -> EmuState -> Int
readMem a st = (getDataMem st) IM.! a
writeMem :: Int -> Int -> EmuState -> DataMem
writeMem a v st = IM.insert a (v `mod` 256) (getDataMem st)

checkBranch :: EmuState -> ProgCount -> Op -> [SArg] -> Maybe ProgCount
checkBranch st p op args = case op of
    OpBeq                           -> doBranch (==)
    OpBne                           -> doBranch (/=)
    OpBlt                           -> doBranch (<)
    OpJmp | [SProgCount p'] <- args -> Just p'
    _                               -> Nothing
    where
        toSigned v = if (v > 127) then v - 256 else v
        doBranch f = case args of
            [SRegLit ra, SRegLit rb, SProgDelta d] -> let a = toSigned $ readReg ra st
                                                          b = toSigned $ readReg rb st
                                                      in if a `f` b then Just (p+d) else Nothing
            _ -> error "emu: invalid branch args"

checkRegisters :: EmuState -> Op -> [SArg] -> RegFile
checkRegisters st op args = case op of
    OpAdd                                -> try3RegOp (+)
    OpSub                                -> try3RegOp (-)
    OpRio | [SRegLit rt] <- args         -> let v = fromMaybe 0 (listToMaybe iobuf) in writeReg rt v st
    OpLdi | [SRegLit rt, SImm n] <- args -> writeReg rt n st
    OpLw                                 -> try2LoadOp
    OpLwl                                -> try2LoadOp
    _ -> rf
    where
        rf = getRegisters st
        iobuf = getIOBuf st
        try3RegOp f = case args of
            [SRegLit rt, SRegLit ra, SRegLit rb] -> writeReg rt (readReg ra st `f` readReg rb st) st
            _ -> error "emu: invalid reg op args"
        try2LoadOp = case args of
            [SRegLit rdst, SRegLit rind] -> writeReg rdst (readMem (readReg rind st) st) st
            _ -> error "emu: invalid load args"

checkDataMem :: EmuState -> Op -> [SArg] -> DataMem
checkDataMem st op args = case op of 
    OpSw  -> try2StoreOp
    OpSwu -> try2StoreOp
    _     -> getDataMem st
    where
        try2StoreOp = case args of
            [SRegLit rval, SRegLit rind] -> writeMem (readReg rind st) (readReg rval st) st
            _ -> error "emu: invalid store args"

prettyState :: EmuState -> String
prettyState (EmuState pc rf im dm io) = unlines
    [printf "PC = %04v" pc
    ,printf "instruction = (%v)" instPretty
    ,printf "Register file:\n%v" (unwords . fmap show3 . MM.elems $ rf)
    ,printf "IO buffer = \"%v\"" (fmap chr io)
    ,printf "Data memory:"
    ,unlines $ zipWith (\a b -> show3 a ++ ": " ++ b) [0,16..] (fmap (unwords . fmap show3) . splitn 16 . IM.elems $ dm)]
    where
        show3 = printf "%03v"
        instPretty = case im IM.!? pc of Just (ExInf _ (InstStmt op args)) -> unwords ((show op):(fmap show args))
                                         Nothing                           -> "No instruction at this address"
        splitn n = takeWhile (not.null) . fmap (take n) . iterate (drop n)