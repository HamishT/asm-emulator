module Util where

import Data.Bifunctor
import Data.List (null, drop)
import Data.Either (partitionEithers)
import Data.Maybe (listToMaybe)

eitherPartition :: [Either a b] -> Either [a] [b]
eitherPartition es = let (ls,rs) = partitionEithers es in if null ls then Right rs else Left ls

readInt :: String -> Maybe Int
readInt = fmap fst . listToMaybe . reads

intRange :: Int -> Int -> Int -> Bool
intRange l u x = x >= l && x <= u

readIntR :: Int -> Int -> String -> Maybe Int
readIntR l u s | Just n <- readInt s, intRange l u n = Just n
readIntR _ _ _ = Nothing

(!?) :: [a] -> Int -> Maybe a
l !? i | i < 0     = Nothing
       | otherwise = listToMaybe (drop i l)

listDelete :: Int -> [a] -> [a]
listDelete i = uncurry (++) . second (drop 1) . splitAt i