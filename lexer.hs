{-# LANGUAGE LambdaCase #-}

module Lexer (Token(..), asmLex) where

import Defs
import Util

import Data.Char
import Control.Applicative

data Token = TkRegVar Ident
           | TkRegLit Reg
           | TkLabel Ident
           | TkNumLit Int
           | TkOpcode Op
           | TkLineSep
           deriving Show

asmLex :: String -> Either [Error] [Token]
asmLex = eitherPartition . concat . fmap processLine . lines
    where
        processLine = tokenise . words . filterComments
        tokenise = (++[Right TkLineSep]) . fmap lexWord
        filterComments = fst . break (=='#')
       

lexWord :: String -> Either Error Token
lexWord = \case
    []                                             -> error "Tried to lex null token"
    ('$':cs) | all isAlphaOrUnderscore cs          -> Right $ TkRegVar cs
             | otherwise                           -> Left  $ "Illegal variable name: must be [a-zA-Z_]"
    ('%':cs) | all isAlphaOrUnderscore cs          -> Right $ TkLabel cs
             | otherwise                           -> Left  $ "Illegal variable name: must be [a-zA-Z_]"
    ('!':cs) | Just n <- readIntR 0 255 cs         -> Right $ TkNumLit n
             | otherwise                           -> Left  $ "Numeric string in range 0..255 expected"
    s@(c:_)  | Just reg <- parseReg s              -> Right $ TkRegLit reg
             | any (==c) "TH"                      -> Left  $ "Illegal register literal: must be H[0..1] or T[0..13]"
    s        | Just op <- getOp <$> parseOp s      -> Right $ TkOpcode op
    s -> Left $ "Unrecognised symbol '" ++ s ++ "'"
    where
        isAlphaOrUnderscore = liftA2 (||) isAlphaNum (=='_')
